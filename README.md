# Bingsu.js

Website for Bingsu.js.

## Tech stack

To make this project as easy to edit as possible, it is built using just normal
HTML+CSS+JS+[Vue](https://vuejs.org).

You should be able to view and edit the website by cloning the repository and
opening "public/index.html" on your web browser. No webpack or any extra build
tooling needed.

A simple Node.js script at "scripts/generate-data.js" will parse the data files
in the "data" folder, and generate the file "public/bingsu.js", which will
contain the collected data.
