# How to contribute

## Hold an event

First and foremost, if you go attend a meetup,
[you can start a Bingsu.js](https://bingsu-js.gitlab.io/). Don’t forget to take
a group selfie and share it online!

## Add an event to this page

To add an event to this page, create a `.toml` file inside the "data" folder.

- The file name should contain the date and short name of the event.

- Fill in the contents using this template

  ```toml
  name = "<Event/conference/meetup name>"            # e.g. React Bangkok 3.1.0

    [[party]]
    name = "<Party name>"                            # e.g. Bingsu.mdx
    place = "<Place>"                                # e.g. Hollys Coffee Nana
    url = "https://www.facebook.com/photo.php?..."   # URL of group selfie
  ```

- If a live video is recorded, add the URL to `event[*].party[*].live` section.

Then submit a Merge Request :)
